﻿namespace puuzzle_me
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pct_fermer = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pct_deplacement = new System.Windows.Forms.PictureBox();
            this.pct_reduire = new System.Windows.Forms.PictureBox();
            this.trace1 = new System.Windows.Forms.PictureBox();
            this.trace4 = new System.Windows.Forms.PictureBox();
            this.trace5 = new System.Windows.Forms.PictureBox();
            this.pct_vide = new System.Windows.Forms.PictureBox();
            this.trace7 = new System.Windows.Forms.PictureBox();
            this.trace8 = new System.Windows.Forms.PictureBox();
            this.trace6 = new System.Windows.Forms.PictureBox();
            this.trace2 = new System.Windows.Forms.PictureBox();
            this.trace3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lab_move = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lab_compteur = new System.Windows.Forms.Label();
            this.pct_puzzle_me = new System.Windows.Forms.PictureBox();
            this.pct_begin = new System.Windows.Forms.PictureBox();
            this.pct_play_pause = new System.Windows.Forms.PictureBox();
            this.pct_play = new System.Windows.Forms.PictureBox();
            this.txt_save = new System.Windows.Forms.TextBox();
            this.pct_save = new System.Windows.Forms.PictureBox();
            this.pct_retour = new System.Windows.Forms.PictureBox();
            this.pct_actualiser = new System.Windows.Forms.PictureBox();
            this.pct_carton = new System.Windows.Forms.PictureBox();
            this.data_liste_score = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pct_fermer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_deplacement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_reduire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_vide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_puzzle_me)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_begin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_play_pause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_play)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_retour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_actualiser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_carton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.data_liste_score)).BeginInit();
            this.SuspendLayout();
            // 
            // pct_fermer
            // 
            this.pct_fermer.BackColor = System.Drawing.Color.Transparent;
            this.pct_fermer.BackgroundImage = global::puuzzle_me.Properties.Resources.fermer_down;
            this.pct_fermer.Location = new System.Drawing.Point(531, 18);
            this.pct_fermer.Name = "pct_fermer";
            this.pct_fermer.Size = new System.Drawing.Size(54, 50);
            this.pct_fermer.TabIndex = 0;
            this.pct_fermer.TabStop = false;
            this.pct_fermer.Click += new System.EventHandler(this.pct_fermer_Click);
            this.pct_fermer.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pct_fermer_MouseDown);
            this.pct_fermer.MouseLeave += new System.EventHandler(this.pct_fermer_MouseLeave);
            this.pct_fermer.MouseHover += new System.EventHandler(this.pct_fermer_MouseHover);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pct_deplacement
            // 
            this.pct_deplacement.BackColor = System.Drawing.Color.Transparent;
            this.pct_deplacement.Location = new System.Drawing.Point(2, 1);
            this.pct_deplacement.Name = "pct_deplacement";
            this.pct_deplacement.Size = new System.Drawing.Size(600, 17);
            this.pct_deplacement.TabIndex = 1;
            this.pct_deplacement.TabStop = false;
            this.pct_deplacement.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pct_deplacement_MouseDown);
            this.pct_deplacement.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pct_deplacement_MouseUp);
            // 
            // pct_reduire
            // 
            this.pct_reduire.BackColor = System.Drawing.Color.Transparent;
            this.pct_reduire.BackgroundImage = global::puuzzle_me.Properties.Resources.reduire_down;
            this.pct_reduire.Location = new System.Drawing.Point(475, 18);
            this.pct_reduire.Name = "pct_reduire";
            this.pct_reduire.Size = new System.Drawing.Size(54, 50);
            this.pct_reduire.TabIndex = 2;
            this.pct_reduire.TabStop = false;
            this.pct_reduire.Click += new System.EventHandler(this.pct_reduire_Click);
            this.pct_reduire.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pct_reduire.MouseLeave += new System.EventHandler(this.pct_reduire_MouseLeave);
            this.pct_reduire.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // trace1
            // 
            this.trace1.BackColor = System.Drawing.Color.Transparent;
            this.trace1.BackgroundImage = global::puuzzle_me.Properties.Resources.pion1_up;
            this.trace1.Location = new System.Drawing.Point(19, 87);
            this.trace1.Name = "trace1";
            this.trace1.Size = new System.Drawing.Size(187, 187);
            this.trace1.TabIndex = 3;
            this.trace1.TabStop = false;
            this.trace1.Visible = false;
            this.trace1.Click += new System.EventHandler(this.trace1_Click);
            this.trace1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trace1_MouseDown);
            this.trace1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trace1_MouseUp);
            // 
            // trace4
            // 
            this.trace4.BackColor = System.Drawing.Color.Transparent;
            this.trace4.BackgroundImage = global::puuzzle_me.Properties.Resources.pion4_up;
            this.trace4.Location = new System.Drawing.Point(20, 277);
            this.trace4.Name = "trace4";
            this.trace4.Size = new System.Drawing.Size(187, 187);
            this.trace4.TabIndex = 4;
            this.trace4.TabStop = false;
            this.trace4.Visible = false;
            this.trace4.Click += new System.EventHandler(this.trace4_Click);
            this.trace4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trace4_MouseDown);
            this.trace4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trace4_MouseUp);
            // 
            // trace5
            // 
            this.trace5.BackColor = System.Drawing.Color.Transparent;
            this.trace5.BackgroundImage = global::puuzzle_me.Properties.Resources.pion5_up;
            this.trace5.Location = new System.Drawing.Point(209, 277);
            this.trace5.Name = "trace5";
            this.trace5.Size = new System.Drawing.Size(187, 187);
            this.trace5.TabIndex = 5;
            this.trace5.TabStop = false;
            this.trace5.Visible = false;
            this.trace5.Click += new System.EventHandler(this.trace5_Click);
            this.trace5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trace5_MouseDown);
            this.trace5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trace5_MouseUp);
            // 
            // pct_vide
            // 
            this.pct_vide.BackColor = System.Drawing.Color.Transparent;
            this.pct_vide.Location = new System.Drawing.Point(400, 468);
            this.pct_vide.Name = "pct_vide";
            this.pct_vide.Size = new System.Drawing.Size(187, 187);
            this.pct_vide.TabIndex = 6;
            this.pct_vide.TabStop = false;
            this.pct_vide.Visible = false;
            // 
            // trace7
            // 
            this.trace7.BackColor = System.Drawing.Color.Transparent;
            this.trace7.BackgroundImage = global::puuzzle_me.Properties.Resources.pion7_up;
            this.trace7.Location = new System.Drawing.Point(20, 467);
            this.trace7.Name = "trace7";
            this.trace7.Size = new System.Drawing.Size(187, 187);
            this.trace7.TabIndex = 7;
            this.trace7.TabStop = false;
            this.trace7.Visible = false;
            this.trace7.Click += new System.EventHandler(this.trace7_Click);
            this.trace7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trace7_MouseDown);
            this.trace7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trace7_MouseUp);
            // 
            // trace8
            // 
            this.trace8.BackColor = System.Drawing.Color.Transparent;
            this.trace8.BackgroundImage = global::puuzzle_me.Properties.Resources.pion8_up;
            this.trace8.Location = new System.Drawing.Point(210, 467);
            this.trace8.Name = "trace8";
            this.trace8.Size = new System.Drawing.Size(187, 187);
            this.trace8.TabIndex = 8;
            this.trace8.TabStop = false;
            this.trace8.Visible = false;
            this.trace8.Click += new System.EventHandler(this.trace8_Click);
            this.trace8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trace8_MouseDown);
            this.trace8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trace8_MouseUp);
            // 
            // trace6
            // 
            this.trace6.BackColor = System.Drawing.Color.Transparent;
            this.trace6.BackgroundImage = global::puuzzle_me.Properties.Resources.pion6_up;
            this.trace6.Location = new System.Drawing.Point(399, 277);
            this.trace6.Name = "trace6";
            this.trace6.Size = new System.Drawing.Size(187, 187);
            this.trace6.TabIndex = 9;
            this.trace6.TabStop = false;
            this.trace6.Visible = false;
            this.trace6.Click += new System.EventHandler(this.trace6_Click);
            this.trace6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trace6_MouseDown);
            this.trace6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trace6_MouseUp);
            // 
            // trace2
            // 
            this.trace2.BackColor = System.Drawing.Color.Transparent;
            this.trace2.BackgroundImage = global::puuzzle_me.Properties.Resources.pion2_up;
            this.trace2.Location = new System.Drawing.Point(208, 87);
            this.trace2.Name = "trace2";
            this.trace2.Size = new System.Drawing.Size(187, 187);
            this.trace2.TabIndex = 10;
            this.trace2.TabStop = false;
            this.trace2.Visible = false;
            this.trace2.Click += new System.EventHandler(this.trace2_Click);
            this.trace2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trace2_MouseDown);
            this.trace2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trace2_MouseUp);
            // 
            // trace3
            // 
            this.trace3.BackColor = System.Drawing.Color.Transparent;
            this.trace3.BackgroundImage = global::puuzzle_me.Properties.Resources.pion3_up;
            this.trace3.Location = new System.Drawing.Point(397, 88);
            this.trace3.Name = "trace3";
            this.trace3.Size = new System.Drawing.Size(187, 187);
            this.trace3.TabIndex = 11;
            this.trace3.TabStop = false;
            this.trace3.Visible = false;
            this.trace3.Click += new System.EventHandler(this.trace3_Click);
            this.trace3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trace3_MouseDown);
            this.trace3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trace3_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::puuzzle_me.Properties.Resources.move_temps;
            this.pictureBox1.Location = new System.Drawing.Point(151, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(186, 51);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // lab_move
            // 
            this.lab_move.AutoSize = true;
            this.lab_move.BackColor = System.Drawing.Color.Sienna;
            this.lab_move.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_move.Location = new System.Drawing.Point(228, 29);
            this.lab_move.Name = "lab_move";
            this.lab_move.Size = new System.Drawing.Size(0, 17);
            this.lab_move.TabIndex = 13;
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lab_compteur
            // 
            this.lab_compteur.AutoSize = true;
            this.lab_compteur.BackColor = System.Drawing.Color.Sienna;
            this.lab_compteur.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_compteur.Location = new System.Drawing.Point(228, 46);
            this.lab_compteur.Name = "lab_compteur";
            this.lab_compteur.Size = new System.Drawing.Size(0, 17);
            this.lab_compteur.TabIndex = 14;
            this.lab_compteur.Visible = false;
            // 
            // pct_puzzle_me
            // 
            this.pct_puzzle_me.BackColor = System.Drawing.Color.Transparent;
            this.pct_puzzle_me.BackgroundImage = global::puuzzle_me.Properties.Resources.puzzle_me;
            this.pct_puzzle_me.Location = new System.Drawing.Point(73, 82);
            this.pct_puzzle_me.Name = "pct_puzzle_me";
            this.pct_puzzle_me.Size = new System.Drawing.Size(476, 322);
            this.pct_puzzle_me.TabIndex = 15;
            this.pct_puzzle_me.TabStop = false;
            // 
            // pct_begin
            // 
            this.pct_begin.BackColor = System.Drawing.Color.Transparent;
            this.pct_begin.BackgroundImage = global::puuzzle_me.Properties.Resources.begin_up;
            this.pct_begin.Location = new System.Drawing.Point(86, 435);
            this.pct_begin.Name = "pct_begin";
            this.pct_begin.Size = new System.Drawing.Size(440, 113);
            this.pct_begin.TabIndex = 16;
            this.pct_begin.TabStop = false;
            this.pct_begin.Click += new System.EventHandler(this.pct_begin_Click);
            this.pct_begin.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pct_begin_MouseDown);
            this.pct_begin.MouseLeave += new System.EventHandler(this.pct_begin_MouseLeave);
            this.pct_begin.MouseHover += new System.EventHandler(this.pct_begin_MouseHover);
            // 
            // pct_play_pause
            // 
            this.pct_play_pause.BackColor = System.Drawing.Color.Transparent;
            this.pct_play_pause.BackgroundImage = global::puuzzle_me.Properties.Resources.pause_up;
            this.pct_play_pause.Location = new System.Drawing.Point(18, 18);
            this.pct_play_pause.Name = "pct_play_pause";
            this.pct_play_pause.Size = new System.Drawing.Size(54, 50);
            this.pct_play_pause.TabIndex = 17;
            this.pct_play_pause.TabStop = false;
            this.pct_play_pause.Visible = false;
            this.pct_play_pause.Click += new System.EventHandler(this.pct_play_pause_Click);
            this.pct_play_pause.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pct_play_pause_MouseDown);
            this.pct_play_pause.MouseLeave += new System.EventHandler(this.pct_play_pause_MouseLeave);
            this.pct_play_pause.MouseHover += new System.EventHandler(this.pct_play_pause_MouseHover);
            // 
            // pct_play
            // 
            this.pct_play.BackColor = System.Drawing.Color.Transparent;
            this.pct_play.BackgroundImage = global::puuzzle_me.Properties.Resources.play_up;
            this.pct_play.Location = new System.Drawing.Point(17, 17);
            this.pct_play.Name = "pct_play";
            this.pct_play.Size = new System.Drawing.Size(54, 50);
            this.pct_play.TabIndex = 18;
            this.pct_play.TabStop = false;
            this.pct_play.Visible = false;
            this.pct_play.Click += new System.EventHandler(this.pct_play_Click);
            this.pct_play.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pct_play_MouseDown);
            this.pct_play.MouseLeave += new System.EventHandler(this.pct_play_MouseLeave);
            this.pct_play.MouseHover += new System.EventHandler(this.pct_play_MouseHover);
            // 
            // txt_save
            // 
            this.txt_save.Font = new System.Drawing.Font("Cambria", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_save.Location = new System.Drawing.Point(98, 273);
            this.txt_save.Name = "txt_save";
            this.txt_save.Size = new System.Drawing.Size(417, 120);
            this.txt_save.TabIndex = 19;
            this.txt_save.Visible = false;
            // 
            // pct_save
            // 
            this.pct_save.BackColor = System.Drawing.Color.Transparent;
            this.pct_save.BackgroundImage = global::puuzzle_me.Properties.Resources.sauvegarder_up;
            this.pct_save.Location = new System.Drawing.Point(86, 410);
            this.pct_save.Name = "pct_save";
            this.pct_save.Size = new System.Drawing.Size(440, 113);
            this.pct_save.TabIndex = 20;
            this.pct_save.TabStop = false;
            this.pct_save.Visible = false;
            this.pct_save.Click += new System.EventHandler(this.pct_save_Click);
            this.pct_save.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pct_save_MouseDown);
            this.pct_save.MouseLeave += new System.EventHandler(this.pct_save_MouseLeave);
            this.pct_save.MouseHover += new System.EventHandler(this.pct_save_MouseHover);
            // 
            // pct_retour
            // 
            this.pct_retour.BackColor = System.Drawing.Color.Transparent;
            this.pct_retour.BackgroundImage = global::puuzzle_me.Properties.Resources.Acceuil;
            this.pct_retour.Location = new System.Drawing.Point(419, 18);
            this.pct_retour.Name = "pct_retour";
            this.pct_retour.Size = new System.Drawing.Size(54, 50);
            this.pct_retour.TabIndex = 21;
            this.pct_retour.TabStop = false;
            this.pct_retour.Visible = false;
            this.pct_retour.Click += new System.EventHandler(this.pct_retour_Click);
            // 
            // pct_actualiser
            // 
            this.pct_actualiser.BackColor = System.Drawing.Color.Transparent;
            this.pct_actualiser.BackgroundImage = global::puuzzle_me.Properties.Resources.actualiser;
            this.pct_actualiser.Location = new System.Drawing.Point(73, 18);
            this.pct_actualiser.Name = "pct_actualiser";
            this.pct_actualiser.Size = new System.Drawing.Size(54, 50);
            this.pct_actualiser.TabIndex = 22;
            this.pct_actualiser.TabStop = false;
            this.pct_actualiser.Visible = false;
            this.pct_actualiser.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pct_carton
            // 
            this.pct_carton.BackColor = System.Drawing.Color.Transparent;
            this.pct_carton.BackgroundImage = global::puuzzle_me.Properties.Resources.carton;
            this.pct_carton.Location = new System.Drawing.Point(362, 18);
            this.pct_carton.Name = "pct_carton";
            this.pct_carton.Size = new System.Drawing.Size(54, 50);
            this.pct_carton.TabIndex = 23;
            this.pct_carton.TabStop = false;
            this.pct_carton.Visible = false;
            this.pct_carton.Click += new System.EventHandler(this.pct_carton_Click);
            // 
            // data_liste_score
            // 
            this.data_liste_score.AllowUserToAddRows = false;
            this.data_liste_score.AllowUserToDeleteRows = false;
            this.data_liste_score.BackgroundColor = System.Drawing.Color.White;
            this.data_liste_score.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.data_liste_score.Location = new System.Drawing.Point(146, 224);
            this.data_liste_score.Name = "data_liste_score";
            this.data_liste_score.ReadOnly = true;
            this.data_liste_score.Size = new System.Drawing.Size(333, 205);
            this.data_liste_score.TabIndex = 24;
            this.data_liste_score.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Lime;
            this.label1.Font = new System.Drawing.Font("Cambria", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(364, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 25;
            this.label1.Text = "SCORE";
            this.label1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Turquoise;
            this.BackgroundImage = global::puuzzle_me.Properties.Resources.bg;
            this.ClientSize = new System.Drawing.Size(602, 672);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.data_liste_score);
            this.Controls.Add(this.pct_carton);
            this.Controls.Add(this.pct_actualiser);
            this.Controls.Add(this.pct_retour);
            this.Controls.Add(this.pct_save);
            this.Controls.Add(this.txt_save);
            this.Controls.Add(this.pct_play);
            this.Controls.Add(this.pct_play_pause);
            this.Controls.Add(this.pct_begin);
            this.Controls.Add(this.pct_puzzle_me);
            this.Controls.Add(this.lab_compteur);
            this.Controls.Add(this.lab_move);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.trace3);
            this.Controls.Add(this.trace2);
            this.Controls.Add(this.trace6);
            this.Controls.Add(this.trace8);
            this.Controls.Add(this.trace7);
            this.Controls.Add(this.pct_vide);
            this.Controls.Add(this.trace5);
            this.Controls.Add(this.trace4);
            this.Controls.Add(this.trace1);
            this.Controls.Add(this.pct_reduire);
            this.Controls.Add(this.pct_deplacement);
            this.Controls.Add(this.pct_fermer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pct_fermer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_deplacement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_reduire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_vide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trace3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_puzzle_me)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_begin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_play_pause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_play)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_retour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_actualiser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_carton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.data_liste_score)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pct_fermer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pct_deplacement;
        private System.Windows.Forms.PictureBox pct_reduire;
        private System.Windows.Forms.PictureBox trace1;
        private System.Windows.Forms.PictureBox trace4;
        private System.Windows.Forms.PictureBox trace5;
        private System.Windows.Forms.PictureBox pct_vide;
        private System.Windows.Forms.PictureBox trace7;
        private System.Windows.Forms.PictureBox trace8;
        private System.Windows.Forms.PictureBox trace6;
        private System.Windows.Forms.PictureBox trace2;
        private System.Windows.Forms.PictureBox trace3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lab_move;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lab_compteur;
        private System.Windows.Forms.PictureBox pct_puzzle_me;
        private System.Windows.Forms.PictureBox pct_begin;
        private System.Windows.Forms.PictureBox pct_play_pause;
        private System.Windows.Forms.PictureBox pct_play;
        private System.Windows.Forms.TextBox txt_save;
        private System.Windows.Forms.PictureBox pct_save;
        private System.Windows.Forms.PictureBox pct_retour;
        private System.Windows.Forms.PictureBox pct_actualiser;
        private System.Windows.Forms.PictureBox pct_carton;
        private System.Windows.Forms.DataGridView data_liste_score;
        private System.Windows.Forms.Label label1;
    }
}

