﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Media;

namespace puuzzle_me
{
    public partial class Form1 : Form
    {
        //Initialisation des positions des pillons.
        Point[] pos = new Point[10];
        int compteur;
        int deplacement=0;
        DateTime dtp1;
        

        public Form1()
        {
            InitializeComponent();

            //Fond Transparent
            //this.TransparencyKey = Color.Turquoise;
            //this.BackColor = Color.Turquoise;


            //Intitialisation des positions de base
            Initialiser_Position();
            compter();
            //commencer();
            //position_aleatoire();
        }


        void sauvegarder_score()
        {
            if (txt_save.Visible == true)
            {
                DataTable dt = new DataTable("Score");

                if (File.Exists(("sauvegarde.xml")))
                {

                    dt.ReadXmlSchema(("sauvegarde.xml"));

                    dt.ReadXml(("sauvegarde.xml"));

                    dt.Rows.Add(txt_save.Text, int.Parse(lab_move.Text), lab_compteur.Text);

                    dt.AcceptChanges();

                    dt.WriteXml(("sauvegarde.xml"));

                }
                else
                {
                    //adding up 6 columns to datatable

                    dt.Columns.Add("pseudo", typeof(string));

                    dt.Columns.Add("deplacement", typeof(int));

                    dt.Columns.Add("temps", typeof(string));





                    //add a row in DataTable and insert data from TextBox

                    dt.Rows.Add(txt_save.Text, int.Parse(lab_move.Text), lab_compteur.Text);

                    dt.AcceptChanges();



                    //Write DataTable Data into XML file(XML file are created in project before writing data)

                    dt.WriteXml(("sauvegarde.xml"));

                }
            }

            txt_save.Visible = false;
            pct_save.Visible = false;

            pct_puzzle_me.Visible = true;
            pct_begin.Visible = true;
            
        }

        void pause_continuer()
        {
            if (trace1.Visible == true)
            {
                timer2.Enabled = false;

                pct_play.Visible = true;
                pct_play_pause.Visible = false;

                pct_actualiser.Visible = false;


                pictureBox1.Visible = true;
                lab_compteur.Visible = true;
                lab_move.Visible = true;
                trace1.Visible = false;
                trace2.Visible = false;
                trace3.Visible = false;
                trace4.Visible = false;
                trace5.Visible = false;
                trace6.Visible = false;
                trace7.Visible = false;
                trace8.Visible = false;

            }
            else 
            {
                timer2.Enabled = true;

                pct_play.Visible = false;
                pct_play_pause.Visible = true;
                pct_actualiser.Visible = true;


                pictureBox1.Visible = true;
                lab_compteur.Visible = true;
                lab_move.Visible = true;
                trace1.Visible = true;
                trace2.Visible = true;
                trace3.Visible = true;
                trace4.Visible = true;
                trace5.Visible = true;
                trace6.Visible = true;
                trace7.Visible = true;
                trace8.Visible = true;
            }
        }


        void gagner()
        {
            if ((trace1.Location == pos[1]) && (trace2.Location == pos[4]) && (trace3.Location == pos[7]) && (trace4.Location == pos[2]) && (trace5.Location == pos[5]) &&(trace6.Location == pos[8]) &&(trace7.Location == pos[3]) && (trace8.Location == pos[6]))
            {
                SoundPlayer sound = new
                SoundPlayer(Properties.Resources.win);
                sound.Play();
                MessageBox.Show("Bien Joué: Partie Terminée");


                trace1.Visible = false;
                trace2.Visible = false;
                trace3.Visible = false;
                trace4.Visible = false;
                trace5.Visible = false;
                trace6.Visible = false;
                trace7.Visible = false;
                trace8.Visible = false;
                pct_actualiser.Visible = false;

                pictureBox1.Visible = false;
                pct_play.Visible = false;
                pct_play_pause.Visible = false;
                lab_compteur.Visible = false;
                lab_move.Visible = false;

                txt_save.Visible = true;
                pct_save.Visible = true;
            }


            //Après Transmission dans la base de donnée
            //compteur = 0;
            //deplacement = 0;
            //dtp1 = DateTime.Now;
            //timer2.Enabled = false;
        }


        void commencer()
        {
            lab_compteur.Text = "";
            lab_move.Text = "";
            compteur = 0;
            deplacement = 0;
            dtp1 = DateTime.Now;
            position_aleatoire();
            timer2.Enabled = true;
            

            pct_puzzle_me.Visible = false;
            pct_begin.Visible = false;
            pct_play_pause.Visible = true;
            pct_actualiser.Visible = true;
            pct_actualiser.Visible = true;
            label1.Visible = true;
            pct_carton.Visible = true;
            pct_retour.Visible = true;

            pictureBox1.Visible = true;
            lab_compteur.Visible = true;
            lab_move.Visible = true;
            trace1.Visible = true;
            trace2.Visible = true;
            trace3.Visible = true;
            trace4.Visible = true;
            trace5.Visible = true;
            trace6.Visible = true;
            trace7.Visible = true;
            trace8.Visible = true;
        }

        void compter()
        {
            //DateTime tempo;
            DateTime dtp2 = DateTime.Now;

            compteur = dtp2.Subtract(dtp1).Seconds;
            int compteur_minute = dtp2.Subtract(dtp1).Minutes;
            int compteur_heure = dtp2.Subtract(dtp1).Hours;

            lab_compteur.Text = compteur_heure.ToString() + "h :" + compteur_minute.ToString() + "min :" + compteur.ToString()+" s";

        }


        void bruitage_cliquer()
        {
            SoundPlayer sound = new
            SoundPlayer(Properties.Resources.click);
            sound.Play();
        
        }


        void  bouger()
        {

          

            SoundPlayer sound = new
            SoundPlayer(Properties.Resources.pion_click);
            sound.Play();

            deplacement = deplacement + 1;

            lab_move.Text = deplacement.ToString();
            gagner();
        }
        


        //Initialisation position de deplacement
        void Initialiser_Position()
        {


            int val = 1;
            for (int i = 19; i <= 395; i = i + 188)
            {
                for (int j = 87; j <= 464; j = j + 187)
                {

                    pos[val] = new Point(i, j);
                    val++;
                }
            }

       


        }


        //Determination position Element Blanc
        int Position_Blanc()
        {

            int cpte = 0;
            for (int i = 1; i <= 9; i++)
            {
                if (pct_vide.Location == pos[i])
                    cpte = i;


            }
            return cpte;
        }


        //Position_Pillon
        int Position_Pillon(PictureBox pillons)
        {
            int i = 0;
            do
            {

                i++;
            }
            while (pillons.Location != pos[i]);
            return (i);
        }



        //:::::::::::::::::::::Gestion des déplacements
        //Gestion du deplacement des Pillons
        void Gest_deplacement(PictureBox pillon)
        {
            int position = Position_Pillon(pillon);
            switch (position)
            {
                case 1:
                    Deplacement_Coin(pillon, 2, 4);
                    break;
                case 3:
                    Deplacement_Coin(pillon, 2, 6);
                    break;
                case 7:
                    Deplacement_Coin(pillon, 4, 8);
                    break;
                case 9:
                    Deplacement_Coin(pillon, 6, 8);
                    break;
                case 2:
                    Deplacement_Cote(pillon, 1, 3, 5);
                    break;
                case 4:
                    Deplacement_Cote(pillon, 1, 5, 7);
                    break;
                case 6:
                    Deplacement_Cote(pillon, 3, 5, 9);
                    break;
                case 8:
                    Deplacement_Cote(pillon, 5, 7, 9);
                    break;
                case 5:
                    Deplacement_Centre(pillon, 2, 4, 6, 8);
                    break;



            }

        }

        //Deplacement du coin
        void Deplacement_Coin(PictureBox selectionner, int val1, int val2)
        {
            if (selectionner.Location == pos[Position_Pillon(selectionner)])
            {
                int temp;
                if (pct_vide.Location == pos[val1])
                {
                    temp = Position_Pillon(selectionner);
                    selectionner.Location = pos[val1];
                    pct_vide.Location = pos[temp];
                    bouger();
                }
                else if (pct_vide.Location == pos[val2])
                {

                    temp = Position_Pillon(selectionner);
                    selectionner.Location = pos[val2];
                    pct_vide.Location = pos[temp];
                    bouger();

                }
            }
        }



        //Deplacement du coté
        void Deplacement_Cote(PictureBox selectionner, int val1, int val2, int val3)
        {
            int temp;
            if (selectionner.Location == pos[Position_Pillon(selectionner)])
            {

                if (pct_vide.Location == pos[val1])
                {
                    temp = Position_Pillon(selectionner);
                    selectionner.Location = pos[val1];
                    pct_vide.Location = pos[temp];
                    bouger();
                }
                else if (pct_vide.Location == pos[val2])
                {

                    temp = Position_Pillon(selectionner);
                    selectionner.Location = pos[val2];
                    pct_vide.Location = pos[temp];
                    bouger();

                }
                else if (pct_vide.Location == pos[val3])
                {

                    temp = Position_Pillon(selectionner);
                    selectionner.Location = pos[val3];
                    pct_vide.Location = pos[temp];
                    bouger();

                }
            }
        }


        //Déplacement du Centre
        void Deplacement_Centre(PictureBox selectionner, int val1, int val2, int val3, int val4)
        {
            int temp;
            if (selectionner.Location == pos[Position_Pillon(selectionner)])
            {

                if (pct_vide.Location == pos[val1])
                {
                    temp = Position_Pillon(selectionner);
                    selectionner.Location = pos[val1];
                    pct_vide.Location = pos[temp];
                    bouger();
                }
                else if (pct_vide.Location == pos[val2])
                {

                    temp = Position_Pillon(selectionner);
                    selectionner.Location = pos[val2];
                    pct_vide.Location = pos[temp];
                    bouger();

                }
                else if (pct_vide.Location == pos[val3])
                {

                    temp = Position_Pillon(selectionner);
                    selectionner.Location = pos[val3];
                    pct_vide.Location = pos[temp];
                    bouger();
                    

                }
                else if (pct_vide.Location == pos[val4])
                {

                    temp = Position_Pillon(selectionner);
                    selectionner.Location = pos[val4];
                    pct_vide.Location = pos[temp];
                    bouger();

                }
            }
        }


        //Procedure d'initialisation aléatoire des objets.
        void position_aleatoire()
        {

            int temp;
            int n;//Nombre Aléatoire.
            int[] tab = new int[10]; //Tableau de entier
            Random aleatoire = new Random();

            //Attributions des valeurs du tableau
            for (int i = 1; i <= 8; i++)
            {


                tab[i] = i ;

            }


            //Tirage sans remise avec diminution progressive de l'intervalle
            for (int i = 1; i <= 8; i++)
            { 
                n=aleatoire.Next(i,9);
                temp = tab[n];
                tab[n] = tab[i];
                tab[i] = temp;
            
            }

            //Attribution des position aléatoires
            trace1.Location=pos[tab[1]];
            trace2.Location=pos[tab[2]];
            trace3.Location=pos[tab[3]];
            trace4.Location=pos[tab[4]];
            trace5.Location=pos[tab[5]];
            trace6.Location=pos[tab[6]];
            trace7.Location=pos[tab[7]];
            trace8.Location = pos[tab[8]];
            pct_vide.Location = pos[9];

        
        }






        //Procedure basique
        private void pct_fermer_Click(object sender, EventArgs e)
        {
            bruitage_cliquer();
            Close();
        }

        private void pct_fermer_MouseHover(object sender, EventArgs e)
        {
            pct_fermer.Image = Properties.Resources.fermer_up;
        }

        private void pct_fermer_MouseLeave(object sender, EventArgs e)
        {
            pct_fermer.Image = Properties.Resources.fermer_down;
        }

        private void pct_fermer_MouseDown(object sender, MouseEventArgs e)
        {
            pct_fermer.Image = Properties.Resources.fermer_down;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int exe = formloc.X - cursloc.X + Cursor.Position.X;
            int eye = formloc.Y - cursloc.Y + Cursor.Position.Y;
            this.Location = new Point(exe, eye);
        }

        private void pct_deplacement_MouseDown(object sender, MouseEventArgs e)
        {
            timer1.Start();
            setposition();
        }

        private void pct_deplacement_MouseUp(object sender, MouseEventArgs e)
        {
            timer1.Stop();
            setposition();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            pct_reduire.Image = Properties.Resources.reduire_down;
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            pct_reduire.Image = Properties.Resources.reduire_up;
        }

        private void pct_reduire_Click(object sender, EventArgs e)
        {
            bruitage_cliquer();
            this.WindowState = FormWindowState.Minimized;
        }

        private void pct_reduire_MouseLeave(object sender, EventArgs e)
        {
            pct_reduire.Image = Properties.Resources.reduire_down;
        }

        private void trace1_Click(object sender, EventArgs e)
        {
            Gest_deplacement(trace1);
            
        }

        private void trace2_Click(object sender, EventArgs e)
        {
            Gest_deplacement(trace2);
        }

        private void trace3_Click(object sender, EventArgs e)
        {
            Gest_deplacement(trace3);
        }

        private void trace4_Click(object sender, EventArgs e)
        {
            Gest_deplacement(trace4);
        }

        private void trace5_Click(object sender, EventArgs e)
        {
            Gest_deplacement(trace5);
        }

        private void trace6_Click(object sender, EventArgs e)
        {
            Gest_deplacement(trace6);
        }

        private void trace7_Click(object sender, EventArgs e)
        {
            Gest_deplacement(trace7);
        }




        //Gestion Affichage a partir du design concu
        const int WS_CLIPCHILDREN = 0x2000000;
        const int WS_MINIZIZEBOX = 0x20000;
        const int WS_MAXIMIZEBOX = 0x10000;
        const int WS_SYSMENU = 0x80000;
        const int CS_DBLCLCKS = 0x8;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style = WS_CLIPCHILDREN | WS_MINIZIZEBOX | WS_SYSMENU;
                cp.ClassStyle = CS_DBLCLCKS;
                return cp;
            }
        }


        Point formloc, cursloc = new Point(0, 0);

        private void setposition()
        {
            formloc = this.Location;
            cursloc = Cursor.Position;
        }



        private void trace8_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(Position_Pillon(trace8).ToString());
            Gest_deplacement(trace8);
        }

        private void trace1_MouseDown(object sender, MouseEventArgs e)
        {
            trace1.Image = Properties.Resources.pion1_down;
        }

        private void trace1_MouseUp(object sender, MouseEventArgs e)
        {
            trace1.Image = Properties.Resources.pion1_up;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            compter();
        }

        private void trace2_MouseDown(object sender, MouseEventArgs e)
        {
            trace2.Image = Properties.Resources.pion2_down;
        }

        private void trace2_MouseUp(object sender, MouseEventArgs e)
        {
            trace2.Image = Properties.Resources.pion2_up;
        }

        private void trace3_MouseDown(object sender, MouseEventArgs e)
        {
            trace3.Image = Properties.Resources.pion3_down;
        }

        private void trace3_MouseUp(object sender, MouseEventArgs e)
        {
            trace3.Image = Properties.Resources.pion3_up;
        }

        private void trace4_MouseDown(object sender, MouseEventArgs e)
        {
            trace4.Image = Properties.Resources.pion4_down;
        }

        private void trace4_MouseUp(object sender, MouseEventArgs e)
        {
            trace4.Image = Properties.Resources.pion4_up;
        }

        private void trace5_MouseDown(object sender, MouseEventArgs e)
        {
            trace5.Image = Properties.Resources.pion5_down;
        }

        private void trace5_MouseUp(object sender, MouseEventArgs e)
        {
            trace5.Image = Properties.Resources.pion5_up;
        }

        private void trace6_MouseDown(object sender, MouseEventArgs e)
        {
            trace6.Image = Properties.Resources.pion6_down;
        }

        private void trace6_MouseUp(object sender, MouseEventArgs e)
        {
            trace6.Image = Properties.Resources.pion6_up;
        }

        private void trace7_MouseDown(object sender, MouseEventArgs e)
        {
            trace7.Image = Properties.Resources.pion7_down;
        }

        private void trace7_MouseUp(object sender, MouseEventArgs e)
        {
            trace7.Image = Properties.Resources.pion7_up;
        }

        private void trace8_MouseDown(object sender, MouseEventArgs e)
        {
            trace8.Image = Properties.Resources.pion8_down;
        }

        private void trace8_MouseUp(object sender, MouseEventArgs e)
        {
            trace8.Image = Properties.Resources.pion8_up;
        }

        private void pct_begin_MouseDown(object sender, MouseEventArgs e)
        {
            pct_begin.Image = Properties.Resources.begin_up;
        }

        private void pct_begin_MouseLeave(object sender, EventArgs e)
        {
            pct_begin.Image = Properties.Resources.begin_up;
        }

        private void pct_begin_MouseHover(object sender, EventArgs e)
        {
            pct_begin.Image = Properties.Resources.begin_down;
        }

        private void pct_begin_Click(object sender, EventArgs e)
        {
            bruitage_cliquer();
            commencer();
        }

        private void pct_play_pause_MouseHover(object sender, EventArgs e)
        {
            pct_play_pause.Image = Properties.Resources.pause_down;
        }

        private void pct_play_pause_MouseLeave(object sender, EventArgs e)
        {
            pct_play_pause.Image = Properties.Resources.pause_up;
        }

        private void pct_play_pause_MouseDown(object sender, MouseEventArgs e)
        {
            pct_play_pause.Image = Properties.Resources.pause_up;
        }

        private void pct_play_MouseDown(object sender, MouseEventArgs e)
        {
            pct_play.Image = Properties.Resources.play_up;
        }

        private void pct_play_MouseHover(object sender, EventArgs e)
        {
            pct_play.Image = Properties.Resources.play_down;
        }

        private void pct_play_MouseLeave(object sender, EventArgs e)
        {
            pct_play.Image = Properties.Resources.play_up;
        }

        private void pct_play_Click(object sender, EventArgs e)
        {
            bruitage_cliquer();
            pause_continuer();
        }

        private void pct_play_pause_Click(object sender, EventArgs e)
        {
            bruitage_cliquer();
            pause_continuer();
        }

        private void pct_save_MouseDown(object sender, MouseEventArgs e)
        {
            pct_save.Image = Properties.Resources.sauvegarder_up;
        }

        private void pct_save_MouseHover(object sender, EventArgs e)
        {
            pct_save.Image = Properties.Resources.sauvegarder_down;
        }

        private void pct_save_MouseLeave(object sender, EventArgs e)
        {
            pct_save.Image = Properties.Resources.sauvegarder_up;
        }

        private void pct_save_Click(object sender, EventArgs e)
        {
            bruitage_cliquer();
            sauvegarder_score();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            bruitage_cliquer();
            position_aleatoire();
        }

        private void pct_retour_Click(object sender, EventArgs e)
        {
            bruitage_cliquer();
            trace1.Visible = false;
            trace2.Visible = false;
            trace3.Visible = false;
            trace4.Visible = false;
            trace5.Visible = false;
            trace6.Visible = false;
            trace7.Visible = false;
            trace8.Visible = false;
            pct_actualiser.Visible = false;

            pictureBox1.Visible = false;
            pct_play.Visible = false;
            pct_play_pause.Visible = false;
            lab_compteur.Visible = false;
            lab_move.Visible = false;
            txt_save.Visible = false;
            pct_save.Visible = false;

            pct_puzzle_me.Visible = true;
            pct_begin.Visible = true;

        }

        private void pct_carton_Click(object sender, EventArgs e)
        {
            bruitage_cliquer();
            if (data_liste_score.Visible == false)
            {
                



                //
                data_liste_score.Visible = true;

                DataSet scorLoad = new DataSet();
                scorLoad.ReadXml("sauvegarde.xml");

                data_liste_score.DataSource = scorLoad.Tables[0];
            }
            else
            {
                



                data_liste_score.Visible = false;
            }
        }

        
        
    }
}
